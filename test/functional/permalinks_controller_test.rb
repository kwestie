require File.dirname(__FILE__) + '/../test_helper'
require 'permalinks_controller'

# Re-raise errors caught by the controller.
class PermalinksController; def rescue_action(e) raise e end; end

class PermalinksControllerTest < Test::Unit::TestCase
  fixtures :permalinks, :users

  def setup
    @controller = PermalinksController.new
    @request    = ActionController::TestRequest.new
    @response   = ActionController::TestResponse.new
  end

  def test_should_get_index
    get :index
    assert_response :success
    assert assigns(:permalinks)
  end

  def test_should_get_new
    login_as :quentin
    get :new
    assert_response :success
  end
  
  def test_should_create_permalink
    login_as :quentin
    old_count = Permalink.count
    post :create, :permalink => { :link_name => "test permalink name" }
    assert_equal old_count+1, Permalink.count
    
    assert_redirected_to permalink_path(assigns(:permalink))
  end

  def test_should_show_permalink
    get :show, :id => "cc76abbc-8dac-11dc-a70d-001558c41534"
    assert_response :success
  end

  def test_should_get_edit
    login_as :quentin
    get :edit, :id => "cc76abbc-8dac-11dc-a70d-001558c41534"
    assert_response :success
  end
  
  def test_should_update_permalink
    login_as :quentin
    put :update, :id => "cc76abbc-8dac-11dc-a70d-001558c41534", :permalink => { :link_name => "new test permalink name" }
    assert_redirected_to permalink_path(assigns(:permalink))
  end
  
  def test_should_destroy_permalink
    login_as :quentin
    old_count = Permalink.count
    delete :destroy, :id => "cc76abbc-8dac-11dc-a70d-001558c41534"
    assert_equal old_count-1, Permalink.count
    
    assert_redirected_to permalinks_path
  end
end
