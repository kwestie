require File.dirname(__FILE__) + '/../test_helper'
require 'issues_controller'

# Re-raise errors caught by the controller.
class IssuesController; def rescue_action(e) raise e end; end

class IssuesControllerTest < Test::Unit::TestCase
  fixtures :issues, :users

  def setup
    @controller = IssuesController.new
    @request    = ActionController::TestRequest.new
    @response   = ActionController::TestResponse.new
  end

  def test_should_get_index
    get :index
    assert_response :success
    assert assigns(:issues)
  end

  def test_should_get_new
    login_as :quentin
    get :new
    assert_response :success
  end
  
  def test_should_create_issue
    login_as :quentin
    old_count = Issue.count
    post :create, :issue => { :title => "test title", :description => "test desc" }
    assert_equal old_count+1, Issue.count
    
    assert_redirected_to issue_path(assigns(:issue))
  end

  def test_should_show_issue
    get :show, :id => "8bea423a-8c75-11dc-b66b-001558c41534"
    assert_response :success
  end

  def test_should_get_edit
    login_as :quentin
    get :edit, :id => "8bea423a-8c75-11dc-b66b-001558c41534"
    assert_response :success
  end
  
  def test_should_update_issue
    login_as :quentin
    put :update, :id => "8bea423a-8c75-11dc-b66b-001558c41534", :issue => { :title => "new test title", :description => "new test desc"}
    assert_redirected_to issue_path(assigns(:issue))
  end
  
  def test_should_destroy_issue
    login_as :quentin
    old_count = Issue.count
    delete :destroy, :id => "8bea423a-8c75-11dc-b66b-001558c41534"
    assert_equal old_count-1, Issue.count
    
    assert_redirected_to issues_path
  end
end
