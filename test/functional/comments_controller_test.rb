require File.dirname(__FILE__) + '/../test_helper'
require 'comments_controller'

# Re-raise errors caught by the controller.
class CommentsController; def rescue_action(e) raise e end; end

class CommentsControllerTest < Test::Unit::TestCase
  fixtures :comments, :users

  def setup
    @controller = CommentsController.new
    @request    = ActionController::TestRequest.new
    @response   = ActionController::TestResponse.new
  end

  def test_should_get_index
    get :index
    assert_response :success
    assert assigns(:comments)
  end

  def test_should_create_comment
    login_as :quentin
    old_count = Comment.count
    post :create, :comment => { :description => "test comment desc" }
    assert_equal old_count+1, Comment.count
    
    assert_redirected_to comment_path(assigns(:comment))
  end

  def test_should_show_comment
    get :show, :id => "35751716-8cd6-11dc-bc6d-001558c41534"
    assert_response :success
  end

  def test_should_get_edit
    login_as :quentin
    get :edit, :id => "35751716-8cd6-11dc-bc6d-001558c41534"
    assert_response :success
  end
  
  def test_should_update_comment
    login_as :quentin
    put :update, :id => "35751716-8cd6-11dc-bc6d-001558c41534", :comment => { :description => "new test comment desc" }
    assert_redirected_to comment_path(assigns(:comment))
  end
  
  def test_should_destroy_comment
    login_as :quentin
    old_count = Comment.count
    delete :destroy, :id => "35751716-8cd6-11dc-bc6d-001558c41534"
    assert_equal old_count-1, Comment.count
    
    assert_redirected_to comments_path
  end
end
