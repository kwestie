module ActiveRecord
  # Some changes that steal more directly from the timestamp system and the current system

  # Active Records will automatically record the user who created and/or updated a database objects
  # if fields of the names created_by/created_by are present.
  #
  # This module requires that your user object (which by default is <tt>User</tt> but can be changed
  # using the <tt>user_model_name</tt> method) contains an accessor called <tt>current_user</tt> and
  # is set with the instance of the currently logged in user (typically using a <tt>before_filter</tt> and the
  # session.
  #
  # The functionality can be turned off on a case by case basis by setting the <tt>record_userstamps</tt>
  # property of your ActiveRecord object to false.
  module Userstamp
    def self.included(base)
      super

      base.alias_method_chain :create, :userstamps
      base.alias_method_chain :update, :userstamps

      base.cattr_accessor :record_userstamps, :instance_writer => false
      base.record_userstamps = true

    end

    # This method is an alias for the normal <tt>create</tt> method. This is where we set the <tt>created_by</tt>
    # and <tt>updated_by</tt> attributes. This only happens if the attributes exist for the model, the
    # <tt>record_userstamps</tt> attribute is true, and the user model has the <tt>current_user</tt> set.
    #
    # After we update those attributes we continue by running the normal <tt>create</tt> method where the object
    # is actually validated and saved.
    def create_with_userstamps
      if record_userstamps && ! User.current_user.nil?
        id = User.current_user.id
        write_attribute(:created_by, id) if respond_to?(:created_by) && self.created_by.nil?
        write_attribute(:updated_by, id) if respond_to?(:updated_by)
      end
      create_without_userstamps
    end

    # This method is an alias for the normal <tt>update</tt> method. This is where we set the <tt>updated_by</tt>
    # attribute. This only happens if the attributes exist for the model, the <tt>record_userstamps</tt>
    # attribute is true, and the user model has the <tt>current_user</tt> set.
    #
    # After we update those attributes we continue by running the normal <tt>update</tt> method where the object
    # is actually validated and saved.
    def update_with_userstamps
      if record_userstamps && ! User.current_user.nil?
        id = User.current_user.id
        write_attribute(:updated_by, id) if respond_to?(:updated_by)
      end
      update_without_userstamps
    end
  end

end
