class KwestieMailer < ActionMailer::Base

  # Generic Receive Mail method
  # The goal here is to
  #   1. identify the reporter sending mail
  #   1a. create a user for reporter if no account exists with that mail
  #   2. identify and process the mail as either
  #   2a. a new issue
  #   2b. a comment on a current issue
  def receive(email)

    # 1. identify the sender
    User.current_user = find_user_from_email(email)

    # 2. if we find the issue the email is a comment response, otherwise we have a new issue
    issue = Issue.find_or_create_by_id(issue_id_from_email(email))

    if issue.new_record?
      # 2a. process the email into the issue
      process_issue_from_email(issue, email)
    else
      # 2b. process the email into a comment for this issue
      process_comment_from_email(issue, email)
    end

  end

  def process_comment_from_email(issue, email)
    comment = issue.comments.create( { :description => email.body, :issue_id => issue.id } )

    if email.has_attachments?
      for attachment in email.attachments
        comment.attachments.create({ :uploaded_data => attachment })
      end
    end

    issue.save
  end

  def process_issue_from_email(issue, email)
    issue.update_attributes( { :description => email.body, :title => email.subject } )

    if email.has_attachments?
      for attachment in email.attachments
        issue.attachments.create({ :uploaded_data => attachment })
      end
    end

    issue.save
  end

  private

    #FIXME: this creates a user on the fly but needs a little more love to be ok
    def find_user_from_email(email)
      for mail in email.from_addrs
        user = User.find_or_create_by_mail(mail)
        return user if ! user.nil?
      end
      return nil
    end

    # FIXME: doesn't handle the CC address
    def issue_id_from_email(email)
      for mail in email.to_addrs
        id = mail.spec.split('@')[0].split('+')[1]
        return id if ! id.nil?
      end
      return nil
    end
end
