class IssueMailer < ActionMailer::Base

  default_url_options[:host] = "0.0.0.0:3000"

  def issue_create(issue)
    setup_email(issue)
  end

  protected
    def setup_email(issue)
      user = User.current_user
      @recipients  = "#{user.email}"
      #FIXME: need to grab this domain from the config
      @from        = "issue+#{issue.id}@localhost.localdomain"
      @subject     = "#{issue.title}"
      @sent_on     = Time.now
      @body        = { :issue => issue,
                   :issue_url => nice_permalink_route_url( :year => issue.year, :month => issue.month, :day => issue.day, :permalink => issue.permalink.link_name ),
                   :user_url => user_url(issue.created_by) }
      #@body[:issue_url] = issue_url(issue)
    end
end
