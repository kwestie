class IssueObserver < ActiveRecord::Observer

  def after_create(issue)
    issue.permalink = Permalink.create( { :link_name => issue.title, :issue_id => issue.id } )
    issue.save
    IssueMailer.deliver_issue_create(issue)
  end

end
