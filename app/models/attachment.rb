class Attachment < ActiveRecord::Base
  usesguid

  has_attachment :storage => :file_system, :path_prefix => 'public/attachments',
                 :thumbnails => { :thumb => 'x50' }

  belongs_to :created_by, :class_name => "User", :foreign_key => "created_by"
end
