class Permalink < ActiveRecord::Base
  usesguid

  belongs_to :issue
  belongs_to :created_by, :class_name => "User", :foreign_key => "created_by"

  validates_presence_of :link_name

  validates_associated :issue

  def lonely_link?
    self.find_by_issue_id(self.id).size == 1
  end

  def link_name=(new_link_name)
    if new_link_name.nil?
      new_link_name = ""
    else
      new_link_name = PermalinkFu.escape(new_link_name)
    end

    write_attribute(:link_name, new_link_name)

  end


end
