class Issue < ActiveRecord::Base
  usesguid

  before_destroy { |issue| Permalink.destroy_all "issue_id = '#{issue.id}'" }

  belongs_to :permalink
  belongs_to :created_by, :class_name => "User", :foreign_key => "created_by"

  has_many :comments, :dependent => :destroy, :order => "comments.created_at ASC"
  has_many :all_permalnks, :class_name => "Permalink", :foreign_key => "issue_id", :dependent => :destroy

  has_many :attachments, :class_name => "Attachment", :foreign_key => "issue_id", :dependent => :destroy

  validates_presence_of :title, :description

  def year
    self.created_at.year()
  end

  def month
    self.created_at.strftime("%m") # month() with leading zero
  end

  def day
    self.created_at.strftime("%d") # day() with leading zero
  end

  # FIXME: we should be checking that the title has changed significantly, not just that it has been edited
  def title=(new_title)
    write_attribute(:title, new_title)
    create_permalink
  end

  private
    def create_permalink()
      return if self.id.nil?
      self.permalink = Permalink.create( { :link_name => self.title, :issue_id => self.id } )
      self.save
    end
end
