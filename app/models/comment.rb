class Comment < ActiveRecord::Base
  usesguid

  belongs_to :issue
  belongs_to :created_by, :class_name => "User", :foreign_key => "created_by"

  has_many :attachments, :class_name => "Attachment", :foreign_key => "comment_id", :dependent => :destroy

  validates_presence_of :description

  validates_associated :issue

end
