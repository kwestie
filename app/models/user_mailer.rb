class UserMailer < ActionMailer::Base

  default_url_options[:host] = "0.0.0.0:3000"

  def signup_notification(user)
    setup_email(user)
    @subject    += 'Please activate your new account'
  
                  #"http://#{default_url_options[:host]}/users/activate/#{user.activation_code}"
    @body[:url]  = url_for :controller => 'users', :action => 'activate', :activation_code => user.activation_code
  
  end
  
  def activation(user)
    setup_email(user)
    @subject    += 'Your account has been activated!'
    @body[:url]  = user_url(user)
  end
  
  protected
    def setup_email(user)
      @recipients  = "#{user.email}"
      @from        = "nobody@localhost.localdomain"
      @subject     = "[KWESTIE] "
      @sent_on     = Time.now
      @body[:user] = user
    end
end
