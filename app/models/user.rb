require 'digest/sha1'
class User < ActiveRecord::Base
  usesguid

  cattr_accessor :current_user

  # Virtual attribute for the unencrypted password
  attr_accessor :password

  validates_presence_of     :login, :email,               :if => :not_openid?
  validates_presence_of     :password,                    :if => :password_required?
  validates_presence_of     :password_confirmation,       :if => :password_required?
  validates_length_of       :password, :within => 4..40,  :if => :password_required?
  validates_confirmation_of :password,                    :if => :password_required?
  validates_length_of       :login,    :within => 3..40,  :if => :not_openid?
  validates_length_of       :email,    :within => 3..100, :if => :not_openid?
  validates_uniqueness_of   :login, :email, :case_sensitive => false, :allow_nil => true

  before_save :encrypt_password
  before_create :make_activation_code

  # prevents a user from submitting a crafted form that bypasses activation
  # anything else you want your user to change should be added here.
  attr_accessible :display_name, :login, :email, :password, :password_confirmation, :identity_url
  
  has_many :issues, :class_name => "Issue", :foreign_key => "created_by", :order => "created_at DESC"
  has_many :comments, :class_name => "Comment", :foreign_key => "created_by", :order => "created_at DESC"

  has_many :recent_issues, :class_name => "Issue", :foreign_key => "created_by", :order => "created_at DESC", :limit => 5
  has_many :recent_comments, :class_name => "Comment", :foreign_key => "created_by", :order => "created_at DESC", :group => "issue_id", :limit => 5

  def self.find_or_create_by_mail(mail)
    user = find_by_email(mail.spec)
    return user if ! user.nil?

    d_name = (mail.name.nil?)? mail.spec.split('@')[0] : mail.name
    p_word = Digest::SHA1.hexdigest( Time.now.to_s.split(//).sort_by {rand}.join )[0,7]
    # a full email is probably a better chance for a unique login than cutting it up
    create({ :email => mail.spec, :login => mail.spec, :display_name => d_name, 
      :password => p_word, :password_confirmation => p_word })
  end

  # Activates the user in the database.
  def activate
    @activated = true
    self.activated_at = Time.now.utc
    self.activation_code = nil
    save(false)
  end

  def activated?
    # the existence of an activation code means they have not activated yet
    activation_code.nil?
  end

  # Returns true if the user has just been activated.
  def recently_activated?
    @activated
  end

  # Authenticates a user by their login name and unencrypted password.  Returns the user or nil.
  def self.authenticate(login, password)
    u = find :first, :conditions => ['login = ? and activated_at IS NOT NULL', login] # need to get the salt
    u && u.authenticated?(password) ? u : nil
  end

  # Encrypts some data with the salt.
  def self.encrypt(password, salt)
    Digest::SHA1.hexdigest("--#{salt}--#{password}--")
  end

  # Encrypts the password with the user salt
  def encrypt(password)
    self.class.encrypt(password, salt)
  end

  def authenticated?(password)
    crypted_password == encrypt(password)
  end

  def remember_token?
    remember_token_expires_at && Time.now.utc < remember_token_expires_at 
  end

  # These create and unset the fields required for remembering users between browser closes
  def remember_me
    remember_me_for 2.weeks
  end

  def remember_me_for(time)
    remember_me_until time.from_now.utc
  end

  def remember_me_until(time)
    self.remember_token_expires_at = time
    self.remember_token            = encrypt("#{email}--#{remember_token_expires_at}")
    save(false)
  end

  def forget_me
    self.remember_token_expires_at = nil
    self.remember_token            = nil
    save(false)
  end

  protected
    # before filter 
    def encrypt_password
      return if password.blank?
      self.salt = Digest::SHA1.hexdigest("--#{Time.now.to_s}--#{login}--") if new_record?
      self.crypted_password = encrypt(password)
    end
    
    def password_required?
      not_openid? && (crypted_password.blank? || !password.blank?)
    end
    
    def make_activation_code
      self.activation_code = Digest::SHA1.hexdigest( Time.now.to_s.split(//).sort_by {rand}.join )
    end 

    def not_openid?
      identity_url.blank?
    end


end
