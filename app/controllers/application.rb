# Filters added to this controller apply to all controllers in the application.
# Likewise, all the methods added will be available for all controllers.

class ApplicationController < ActionController::Base
  # Pick a unique cookie name to distinguish our session data from others'
  session :session_key => '_kwestie_session_id'
  layout 'default'

  include AuthenticatedSystem
  before_filter :set_current_user

  #FIXME: this is copy and paste from PermalinksHelper

  def nice_permalink_url(issue)
    nice_permalink_route_url( :year => issue.year, :month => issue.month, :day => issue.day, :permalink => issue.permalink.link_name )
  end
  def nice_permalink_path(issue)
    nice_permalink_route_path( :year => issue.year, :month => issue.month, :day => issue.day, :permalink => issue.permalink.link_name )
  end

  private
    def set_current_user
      User.current_user = current_user
    end

    def redirect_back_or(path)
      redirect_to :back
      rescue ActionController::RedirectBackError
        redirect_to path
    end
end
