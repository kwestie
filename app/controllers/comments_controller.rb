class CommentsController < ApplicationController

  before_filter :login_required, :only => [ :new, :create, :edit, :update, :destroy]

  # GET /comments
  # GET /comments.xml
  def index
    @comments = Comment.find(:all)

    respond_to do |format|
      format.html { render :xml => @comments.to_xml }
      format.xml  { render :xml => @comments.to_xml }
    end
  end

  # GET /comments/1 - comments are not accessed individually like this
  # GET /comments/1.xml
  def show
    @comment = Comment.find(params[:id])

    respond_to do |format|
      format.html { render :partial => 'show', :locals => { :comment => @comment } }
      format.xml  { render :xml => @comment.to_xml }
    end
  end

  # GET /comments/1;edit
  def edit
    @comment = Comment.find(params[:id])
  end

  # POST /comments
  # POST /comments.xml
  def create
    @comment = Comment.new(params[:comment])

    respond_to do |format|
      if @comment.save
        flash[:notice] = 'Comment was successfully created.'
        format.html { redirect_to issue_url(@comment.issue)  }
        format.xml  { head :created, :location => comment_url(@comment) }
      else
        format.html { redirect_to issue_url(@comment.issue) }
        format.xml  { render :xml => @comment.errors.to_xml }
      end
    end
  end

  # PUT /comments/1
  # PUT /comments/1.xml
  def update
    @comment = Comment.find(params[:id])

    respond_to do |format|
      if @comment.update_attributes(params[:comment])
        flash[:notice] = 'Comment was successfully updated.'
        format.html { redirect_to comment_url(@comment) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @comment.errors.to_xml }
      end
    end
  end

  # DELETE /comments/1
  # DELETE /comments/1.xml
  def destroy
    @comment = Comment.find(params[:id])
    @comment.destroy

    respond_to do |format|
      format.html { redirect_to comments_url }
      format.xml  { head :ok }
    end
  end
end
