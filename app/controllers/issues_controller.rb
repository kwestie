class IssuesController < ApplicationController

  helper :permalinks
  include ActionView::Helpers::DateHelper

  before_filter :login_required, :only => [ :new, :create, :edit, :update, :destroy, :comment]

  # GET /issues
  # GET /issues.xml
  def index
    @issues = Issue.find(:all, :order => "created_at DESC")

    respond_to do |format|
      format.html # index.rhtml
      format.xml  { render :xml => @issues.to_xml }
    end
  end

  # GET /issues/ID
  # GET /issues/ID.xml
  def show
    @issue = Issue.find(params[:id])

    respond_to do |format|
      format.html { redirect_to nice_permalink_url(@issue) }
      format.xml  { render :xml => @issue.to_xml }
    end
  end

  # GET /issues/YEAR/MO?/DA?
  def find_by_date
    dates = parse_date
    @issues = Issue.find(:all, :conditions => [" created_at BETWEEN ? and ? ", dates["from"], dates["to"] ], 
                              :order => :created_at)
    flash[:message] = "Issues for %s %s from %s" %  [ dates["to"], distance_of_time_in_words( dates["requested"], dates["to"] ), dates["format"] ]
    render :action => "index"
  end

  # GET /issues/YEAR/MO/DA/PERMALINK
  def find_by_permalink
    dates = parse_date
    # FIXME: this finds the correct issue, but won't do the forward correctly so you can't act on an issue you find
#    @issues = Issue.find(:all, :joins => 'LEFT JOIN permalinks ON permalinks.issue_id = issues.id', :conditions => [" (issues.created_at BETWEEN ? and ?) and link_name = ? ", dates["from"], dates["to"], params[:permalink] ] )

    @issues = Issue.find(:all, :include => [ :permalink, :created_by ], :conditions => [" (issues.created_at BETWEEN ? and ?) and link_name = ? ", dates["from"], dates["to"], params[:permalink] ] )

    # FIXME: we should be smarter here where there is more than one issue with the same date and permalink name
    @issue = @issues[0]

    respond_to do |format|
      format.html { render :action => "show" }
      format.xml  { render :xml => @issue.to_xml }
    end
  end

  # GET /issues/YEAR/MO/DA/PERMALINK/ID
  def find_by_permalink_id
    @issue = Issue.find(params[:id])

    respond_to do |format|
      format.html { render  :action => "show" }
      format.xml  { render :xml => @issue.to_xml }
    end
  end

  # GET /issues/new
  def new
    @issue = Issue.new
  end

  # GET /issues/1;edit
  def edit
    @issue = Issue.find(params[:id])
  end

  # GET /issues/comment/1
  def comment
    @issue = Issue.find(params[:id])
    redirect_to issue_url(@issue)
  end

  # POST /issues
  # POST /issues.xml
  def create
    @issue = Issue.new(params[:issue])

    respond_to do |format|
      if @issue.save
        flash[:notice] = 'Issue was successfully created.'
        format.html { redirect_to issue_url(@issue) }
        format.xml  { head :created, :location => issue_url(@issue) }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @issue.errors.to_xml }
      end
    end
  end

  # PUT /issues/1
  # PUT /issues/1.xml
  def update
    @issue = Issue.find(params[:id])

    respond_to do |format|
      if @issue.update_attributes(params[:issue])
        flash[:notice] = 'Issue was successfully updated.'
        format.html { redirect_to issue_url(@issue) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @issue.errors.to_xml }
      end
    end
  end

  # DELETE /issues/1
  # DELETE /issues/1.xml
  def destroy
    @issue = Issue.find(params[:id])
    @issue.destroy

    respond_to do |format|
      format.html { redirect_to issues_url }
      format.xml  { head :ok }
    end
  end

  protected
    def parse_date
      # adapted from http://www.therailsway.com/tags/idiomatic%20ruby

      # here we take in whatever dates we get or use the 1st of the month or year if the date isn't present
      requested_date = Date.new(params[:year].to_i, (params[:month] || 1).to_i, (params[:day] || 1).to_i)

      # Date#- subtracts days from a given date, which lets us find
      # the end of last month.
      if ! params[:day]
        from = requested_date - 1
      else # correct in the ! params[:month] case as well
        from = requested_date
      end

      # Date#>> lets us advance requested date by one month, 
      # giving us the first of the next month.

      if ! params[:day] # year and month
        to = requested_date >> 1
      elsif ! params[:month] # year only
        to = Date.new(requested_date.year + 1, requested_date.month, requested_date.day)
      else # year and month and day
        to = requested_date + 1
      end

      requested_format = requested_date.to_s(:day)
      requested_format = requested_date.to_s(:month) if ! params[:day]
      requested_format = requested_date.to_s(:year)  if ! params[:month]

      return { "to" => to, "from" => from, "requested" => requested_date ,"format" => requested_format }
    end
end
