class UsersController < ApplicationController
  layout 'default'

  # render new.rhtml
  def new
  end

  # GET /users/1
  # GET /users/1.xml
  def show
    @user = User.find(params[:id], :include => [:issues, :comments])

    respond_to do |format|
      format.html # show.rhtml
      format.xml  { render :xml => @user.to_xml }
    end
  end

  def create
    cookies.delete :auth_token
    reset_session
    @user = User.new(params[:user])
    @user.save!
    self.current_user = @user
    redirect_back_or('/')
    flash[:notice] = "Thanks for signing up!"
  rescue ActiveRecord::RecordInvalid
    render :action => 'new'
  end

  def activate
    self.current_user = params[:activation_code].blank? ? :false : User.find_by_activation_code(params[:activation_code])
    if logged_in? && !current_user.activated?
      current_user.activate
      flash[:notice] = "Your account has been activated!! You should check out <a href='#{url_for(user_url(current_user))}'>your page</a>"
    else
      flash[:notice] = "I don't recognize that activation code, perhaps you should try again"
    end
    redirect_to index_url
  end

end
