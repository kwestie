class PermalinksController < ApplicationController
  # GET /permalinks
  # GET /permalinks.xml
  def index
    @permalinks = Permalink.find(:all)

    respond_to do |format|
      format.html # index.rhtml
      format.xml  { render :xml => @permalinks.to_xml }
    end
  end

  # GET /permalinks/1
  # GET /permalinks/1.xml
  def show
    @permalink = Permalink.find(params[:id])

    respond_to do |format|
      format.html # show.rhtml
      format.xml  { render :xml => @permalink.to_xml }
    end
  end

  # GET /permalinks/new
  def new
    @permalink = Permalink.new
  end

  # GET /permalinks/1;edit
  def edit
    @permalink = Permalink.find(params[:id])
  end

  # POST /permalinks
  # POST /permalinks.xml
  def create
    @permalink = Permalink.new(params[:permalink])

    respond_to do |format|
      if @permalink.save
        flash[:notice] = 'Permalink was successfully created.'
        format.html { redirect_to permalink_url(@permalink) }
        format.xml  { head :created, :location => permalink_url(@permalink) }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @permalink.errors.to_xml }
      end
    end
  end

  # PUT /permalinks/1
  # PUT /permalinks/1.xml
  def update
    @permalink = Permalink.find(params[:id])

    respond_to do |format|
      if @permalink.update_attributes(params[:permalink])
        flash[:notice] = 'Permalink was successfully updated.'
        format.html { redirect_to permalink_url(@permalink) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @permalink.errors.to_xml }
      end
    end
  end

  # DELETE /permalinks/1
  # DELETE /permalinks/1.xml
  def destroy
    @permalink = Permalink.find(params[:id])
    @permalink.destroy

    respond_to do |format|
      format.html { redirect_to permalinks_url }
      format.xml  { head :ok }
    end
  end
end
