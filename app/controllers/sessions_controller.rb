# This controller handles the login/logout function of the site.  
class SessionsController < ApplicationController

  # render new.rhtml
  def new
  end

  def create
    if using_open_id?
      open_id_authentication
    else
      password_authentication(params[:login], params[:password])
    end
  end

  def destroy
    self.current_user.forget_me if logged_in?
    cookies.delete :auth_token
    reset_session
    flash[:notice] = "You have been logged out."
    redirect_back_or_default('/')
  end

  protected

    def password_authentication(login, password)
      self.current_user = User.authenticate(login, password)
      if logged_in?
        remember_me_login
        successful_login
      else
        failed_login "Sorry, your login or password are incorrect"
      end
    end

    def open_id_authentication
      # Pass optional :required and :optional keys to specify what sreg fields you want.
      # Be sure to yield registration, a third argument in the #authenticate_with_open_id block.
      authenticate_with_open_id(identity_url, 
      :required => [ :nickname, :email ],
      :optional => :fullname)  do |result, identity_url, registration|
        case status
          when :missing
            failed_login "Sorry, the OpenID server couldn't be found"
          when :canceled
            failed_login "OpenID verification was canceled"
          when :failed
            failed_login "Sorry, the OpenID verification failed"
          when :successful
            if self.current_user = User.find_by_identity_url(identity_url)
              assign_registration_attributes!(registration)
              if self.current_user.save
                successful_login
              else
                failed_login "Your OpenID profile registration failed: " +
                  self.current_user.errors.full_messages.to_sentence
              end
            else
              failed_login "Sorry, no user by that identity URL exists"
            end
        end
      end
    end

    # registration is a hash containing the valid sreg keys given above
    # use this to map them to fields of your user model
    def assign_registration_attributes!(registration)
      model_to_registration_mapping.each do |model_attribute, registration_attribute|
        unless registration[registration_attribute].blank?
          self.current_user.send("#{model_attribute}=", registration[registration_attribute])
        end
      end
    end

    def model_to_registration_mapping
      { :login => 'nickname', :email => 'email', :display_name => 'fullname' }
    end


  private

    def remember_me_login
      if params[:remember_me] == "1"
        self.current_user.remember_me
        cookies[:auth_token] = { :value => self.current_user.remember_token , :expires => self.current_user.remember_token_expires_at }
      end
    end

    def successful_login
      redirect_back_or_default(index_url)
      flash[:notice] = "Welcome!  You've logged in successfully"
    end

    def failed_login(message)
      render :action => 'new'
      #redirect_to(:action => ‘login’)
      flash[:warning] = message
    end
end
