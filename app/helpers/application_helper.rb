# Methods added to this helper will be available to all templates in the application.
module ApplicationHelper
  def form_label(obj, meth, str)
    '<label for="%s_%s">%s</label>' % [ obj, meth, str ]
  end

  def focus(field_name)
    %{
    <script type="text/javascript">
    // <![CDATA[
  	  Field.activate('#{field_name}');
    // ]]>
    </script>
    }
  end

  def hCard(user)
    %{
    <span class="vcard">
      #{link_to user.display_name, user_url(user), :class => "author url fn nickname"}
    </span>
    }
  end

  def nice_permalink_url(issue)
    nice_permalink_route_url( :year => issue.year, :month => issue.month, :day => issue.day, :permalink => issue.permalink.link_name )
  end

  def nice_permalink_path(issue)
    nice_permalink_route_path( :year => issue.year, :month => issue.month, :day => issue.day, :permalink => issue.permalink.link_name )
  end

  # FIXME: we'll probably need to do a bit more sanitizing than this later
  def format_description(desc)
    auto_link(simple_format(desc), :all, :target => '_blank')
  end
end
