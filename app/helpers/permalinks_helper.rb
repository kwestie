module PermalinksHelper
  def nice_permalink_url(issue)
    nice_permalink_route_url( :year => issue.year, :month => issue.month, :day => issue.day, :permalink => issue.permalink.link_name )
  end
  def nice_permalink_path(issue)
    nice_permalink_route_path( :year => issue.year, :month => issue.month, :day => issue.day, :permalink => issue.permalink.link_name )
  end
end
