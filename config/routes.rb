ActionController::Routing::Routes.draw do |map|
  map.resources :attachments


  map.resources :permalinks
  map.resources :comments
  map.resources :issues
  map.resources :users

  # The priority is based upon order of creation: first created -> highest priority.
  
  # Sample of regular route:
  # map.connect 'products/:id', :controller => 'catalog', :action => 'view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  # map.purchase 'products/:id/purchase', :controller => 'catalog', :action => 'purchase'
  # This route can be invoked with purchase_url(:id => product.id)

  # You can have the root of your site routed by hooking up '' 
  # -- just remember to delete public/index.html.
  # map.connect '', :controller => "welcome"

  map.open_id_complete 'session', :controller => "sessions", :action => "create", :requirements => { :method => :get }
  map.resource :session

  map.activate '/activate/:activation_code', :controller => 'users', :action => 'activate'

  map.signup '/signup', :controller => 'users', :action => 'new'
  map.login  '/login', :controller => 'sessions', :action => 'new'
  map.logout '/logout', :controller => 'sessions', :action => 'destroy'

  map.index '', :controller => 'issues', :action => 'index'

  map.date 'dates/:year/:month/:day', :controller  => 'issues', 
       :action => 'find_by_date', 
       :month => nil, :day => nil,
       :requirements => { :year => /\d{4}/, :day => /\d{1,2}/, :month => /\d{1,2}/ }

  map.nice_permalink_route 'issue/:year/:month/:day/:permalink', :controller  => 'issues', 
       :action => 'find_by_permalink',
       :requirements => { :year => /\d{4}/, :day => /\d{2}/, :month => /\d{2}/ }

  map.full_permalink_route 'issue/:year/:month/:day/:permalink/:id', :controller  => 'issues', 
       :action => 'find_by_permalink_id',
       :requirements => { :year => /\d{4}/, :day => /\d{2}/, :month => /\d{2}/ }


  # Allow downloading Web Service WSDL as a file with an extension
  # instead of a file named 'wsdl'
  map.connect ':controller/service.wsdl', :action => 'wsdl'

  # Install the default route as the lowest priority.
  map.connect ':controller/:action/:id.:format'
  map.connect ':controller/:action/:id'
end
