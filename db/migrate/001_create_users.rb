class CreateUsers < ActiveRecord::Migration
  def self.up
    create_table "users", :id => false do |t|
      t.column :id,                        :string, :limit => 36, :null => false
      t.column :login,                     :string
      t.column :email,                     :string
      t.column :display_name,              :string, :default => ""
      t.column :crypted_password,          :string, :limit => 40
      t.column :salt,                      :string, :limit => 40
      t.column :created_at,                :datetime
      t.column :updated_at,                :datetime
      t.column :remember_token,            :string
      t.column :remember_token_expires_at, :datetime
      # User Activation http://technoweenie.stikipad.com/plugins/show/User+Activation
      t.column :activation_code,           :string, :limit => 40
      t.column :activated_at,              :datetime
      # OpenID http://www.bencurtis.com/archives/2007/03/rails-openid-and-acts-as-authenticated/
      t.column :identity_url,              :string
    end
    add_index(:users, [:id], :unique => true, :name => :users_pkey)
    add_index(:users, [:created_at, :updated_at], :name => :users_created)
    add_index(:users, [:display_name, :email, :activation_code, :identity_url], :name => :users_idx)
  end

  def self.down
    remove_index :users, :name => :users_idx
    remove_index :users, :name => :users_created
    remove_index :users, :name => :users_pkey
    drop_table "users"
  end
end
