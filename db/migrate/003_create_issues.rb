class CreateIssues < ActiveRecord::Migration
  def self.up
    create_table :issues, :id => false do |t|
      t.column :id,                     :string, :limit => 36, :null => false
      t.column :permalink_id,           :string, :limit => 36, :null => false
      t.column :title,                  :string
      t.column :description,            :text
      t.column :created_at,             :datetime
      t.column :updated_at,             :datetime
      t.column :created_by,             :string, :limit => 36, :null => false
    end
    add_index(:issues, [:id], :unique => true, :name => :issues_pkey)
    add_index(:issues, [:created_by, :created_at, :updated_at], :name => :issues_created)
    add_index(:issues, [:permalink_id, :title], :name => :issues_idx)
  end

  def self.down
    remove_index :issues, :name => :issues_idx
    remove_index :issues, :name => :issues_created
    remove_index :issues, :name => :issues_pkey
    drop_table :issues
  end
end
