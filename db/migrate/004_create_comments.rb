class CreateComments < ActiveRecord::Migration
  def self.up
    create_table :comments, :id => false  do |t|
      t.column :id,                      :string, :limit => 36, :null => false
      t.column :issue_id,                :string, :limit => 36, :null => false
      t.column :description,             :text
      t.column :created_at,              :datetime
      t.column :created_by,              :string, :limit => 36, :null => false
    end
    add_index(:comments, [:id], :unique => true, :name => :comments_pkey)
    add_index(:comments, [:created_by, :created_at], :name => :comments_created)
    add_index(:comments, [:issue_id], :name => :comments_idx)
  end

  def self.down
    remove_index :comments, :name => :comments_idx
    remove_index :comments, :name => :comments_created
    remove_index :comments, :name => :comments_pkey
    drop_table :comments
  end
end
