class CreatePermalinks < ActiveRecord::Migration
  def self.up
    create_table :permalinks, :id => false  do |t|
      t.column :id,                      :string, :limit => 36, :null => false
      t.column :issue_id,                :string, :limit => 36, :null => false
      t.column :link_name,               :string
      t.column :created_at,              :datetime
      t.column :created_by,              :string, :limit => 36, :null => false
    end
    add_index(:permalinks, [:id], :unique => true, :name => :permalinks_pkey)
    add_index(:permalinks, [:created_by, :created_at], :name => :permalinks_created)
    add_index(:permalinks, [:issue_id, :link_name], :name => :permalinks_idx)
  end

  def self.down
    remove_index :permalinks, :name => :permalinks_idx
    remove_index :permalinks, :name => :permalinks_created
    remove_index :permalinks, :name => :permalinks_pkey
    drop_table :permalinks
  end
end
