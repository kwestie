class CreateAttachments < ActiveRecord::Migration
  def self.up
    create_table :attachments, :id => false  do |t|
      t.column :id,                       :string, :limit => 36, :null => false
      t.column :issue_id,                 :string, :limit => 36
      t.column :comment_id,               :string, :limit => 36
      t.column :created_at,               :datetime
      t.column :created_by,               :string, :limit => 36, :null => false

      # attachment_fu required fields
      t.column :size,                    :integer
      t.column :content_type,            :string
      t.column :filename,                :string

      t.column :height,                  :integer
      t.column :width,                   :integer

      t.column :parent_id,               :string, :limit => 36
      t.column :thumbnail,               :string
    end
    add_index(:attachments, [:id], :unique => true, :name => :attachments_pkey)
    add_index(:attachments, [:created_by, :created_at], :name => :attachments_created)
    add_index(:attachments, [:issue_id, :comment_id, :parent_id], :name => :attachments_idx)
  end

  def self.down
    remove_index :attachments, :name => :attachments_idx
    remove_index :attachments, :name => :attachments_created
    remove_index :attachments, :name => :attachments_pkey
    drop_table :attachments
  end
end
